#include "util.h"



char* concat(int count, ...)
{
    va_list ap;
    int i;

    // Find required length to store merged string
    int len = 1; // room for NULL
    va_start(ap, count);
    for(i=0 ; i<count ; i++)
    {
        len += strlen(va_arg(ap, char*));
    }
    va_end(ap);

    // Allocate memory to concat strings
    char *merged = calloc(sizeof(char),len);
    int null_pos = 0;

    // Actually concatenate strings
    va_start(ap, count);
    for(i=0 ; i<count ; i++)
    {
        char *s = va_arg(ap, char*);
        strcpy(merged+null_pos, s);
        null_pos += strlen(s);
    }
    va_end(ap);

    return merged;
}

char *getFileInfo(char * filename) {
	struct stat fileAttr;
	struct tm * tme;
	char mtime[20];
	char fileSize[30];

	if (stat(filename, &fileAttr) < 0) {
		perror("File doesnt exist!");
		return NULL;
	}

	tme = localtime(&(fileAttr.st_mtime));

	if (tme == NULL) {
		perror("localtime");
		exit(1);
	}

	/* format time days.month.year hour:minute:seconds */
	if (strftime(mtime, sizeof(mtime), "%d.%m.%Y %H:%M:%S", tme) == 0) { //%d.%m.%Y %H:%M:%S //%x - %H:%M
		printf("error\n");
		exit(1);
	}
	snprintf(fileSize, sizeof(fileSize), "%lu", fileAttr.st_size);

	FILE *fp;
	long lSize;

	fp = fopen(filename, "rb");
	if (!fp) {
		perror("opening file");
		exit(1);
	}

	fseek(fp, 0L, SEEK_END);
	lSize = ftell(fp);
	rewind(fp);
	char content[lSize];

	//copy file into buffer
	if (1 != fread(content, lSize, 1, fp)) {
		fclose(fp);
		perror("read failed");
	}
	fclose(fp);

	return concat(5, mtime, ",", fileSize, ",", content);
}
