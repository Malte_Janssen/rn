#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <time.h>
#include <sys/stat.h>
#include <stdio.h>


#include "util.h"

#define SRV_PORT	"7777"



int getInput(char* cmd[], char* filename[]) {
	size_t inputSize = 20;
	char input[inputSize];
	char *input_ptr = input;
	memset(input, 0, sizeof(input));

	int chars = getline(&input_ptr, &inputSize, stdin);
	if (chars <= 1) {
		return -1;
	} else if (input[chars - 1] == '\n') {
		input[chars - 1] = '\0';
	}
	*cmd = strtok(input, " ");
	*filename = strtok(NULL, " ");
	return 0;
}

int main(int argc, char * argv[]) {
	if (argc != 3) {
		printf("check arguments, need 2, got :%d\n", argc);
		exit(1);
	}

	int s_tcp; /* socket descriptor */
	struct addrinfo hints, *servArr, *serv;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	if (getaddrinfo(argv[1], argv[2], &hints, &servArr) != 0) {
		perror("Server not found.");
		exit(1);
	}

	for (serv = servArr; serv != NULL; serv = serv->ai_next) {
		s_tcp = socket(serv->ai_family, serv->ai_socktype, serv->ai_protocol);
		if (s_tcp == -1) {
			continue;
		}
		if (connect(s_tcp, serv->ai_addr, serv->ai_addrlen) == 0) {
			printf("Connected \n");
			break; /* Success */
		} else {
			perror("Connection Error");
		}
	}

	while (1) {
		int n;

		char* cmd;
		char* filename;
		char server_reply[256];
		memset(server_reply,0,sizeof server_reply);
		if (getInput(&cmd, &filename) == -1) {
			printf("Didnt receive a valid command!\n");
			continue;
		}

		if (strcmp(cmd, "List") == 0) {

			if ((n = send(s_tcp, cmd, strlen(cmd), 0)) > 0) {
				printf("Message %s sent ( %i Bytes).\n", cmd, n);
			} else {
				printf("Message %s sent ( %i/%i Bytes).\n", cmd, n,
						sizeof(cmd));
			}
		}
		if (strcmp(cmd, "Quit") == 0) {
			if ((n = send(s_tcp, "Quit,", strlen("Quit,"), 0)) > 0) {
				printf("Message %s sent ( %i Bytes).\n", cmd, n);
			}
			close(s_tcp);
			exit(1);
		}
		if (strcmp(cmd, "Put") == 0) {
			if (filename == NULL) {
				printf("Error: didnt received a valid filename\n");
			} else {
				char *fileInfoTemp = getFileInfo(filename);
				char *fileInfo = concat(3,filename,",",fileInfoTemp);
				if (fileInfoTemp == NULL) { //TODO doesnt work
					continue;
				}
				char * message = concat(3, "Put", ",", fileInfo);
				if ((n = send(s_tcp, message, strlen(message), 0)) > 0) {
					printf("Textfile %s sent ( %i/%i Bytes).\n", message, n,
							strlen(message));
				}
				free(message);
				free(fileInfo);
				free(fileInfoTemp);
			}
		}
		if (strcmp(cmd, "Get") == 0) {
			if (filename == NULL) {
				printf("Error: didnt received a valid filename\n");
			} else {
				char * message = concat(3, "Get", ",", filename);
				if ((n = send(s_tcp, message, strlen(message), 0)) > 0) {
					printf("Textfile %s sent ( %i/%i Bytes).\n", message, n,
							strlen(message));
				}
			}
		}
		if (recv(s_tcp, server_reply, sizeof server_reply, 0) < 0) {
			printf("recv failed");
		}else {
			printf("Server reply : %s \n", server_reply);
		}


	}

	close(s_tcp);
	return 0;
}
