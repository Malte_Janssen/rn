#ifndef UTIL_H_
#define UTIL_H_

#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>
#include <sys/stat.h>
#include <stdio.h>

class util {

	/**
	 * Concantinates a variable number of strings
	 * allocates memory!
	 */
public:
	static char* concat(int count, ...);
	static char *getFileInfo(char*);
};
#endif /* UTIL_H_ */
