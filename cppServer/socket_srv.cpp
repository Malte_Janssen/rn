#include <map>
#include <sys/types.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <utime.h>
#include <time.h>
#include <sys/stat.h>
#include <stdio.h>
#include <sys/socket.h>
#include "util.h"

//#define _XOPEN_SOURCE 700
#define SRV_PORT			"6662"
#define NUMBER_OF_CLIENTS	5

char *get_ip_str(const struct sockaddr *sa, char *s, size_t maxlen)
{
	switch (sa->sa_family)
	{
	case AF_INET:
		inet_ntop(AF_INET, &(((struct sockaddr_in *) sa)->sin_addr), s, maxlen);
		break;

	case AF_INET6:
		inet_ntop(AF_INET6, &(((struct sockaddr_in6 *) sa)->sin6_addr), s, maxlen);
		break;

	default:
		strncpy(s, "Unknown AF", maxlen);
		return NULL;
	}

	return s;
}

char *getCurrentTime()
{
	time_t current_time;
	struct tm * time_info;
	char *timeString = (char*) malloc(20);

	time(&current_time);
	time_info = localtime(&current_time);
	strftime(timeString, sizeof(timeString), "%d.%m.%Y %H:%M:%S", time_info);
	return timeString;
}

void saveTxtAsFile(char fileInfo[])
{

	char *seperator = ",";
	char *filename = strtok(fileInfo, seperator);
	char *mtime = strtok(NULL, seperator);
	char *size = strtok(NULL, seperator);
	char *content = strtok(NULL, "");
	struct tm tme;
	struct utimbuf ubuf;

	FILE *file = fopen(filename, "w+");

	if (!file)
	{
		perror("opening file");
		exit(1);
	}

	if (strlen(content) != fwrite(content, 1, strlen(content), file))
	{
		fclose(file);
		perror("write failed");
	}
	fclose(file);

	strptime(mtime, "%d.%m.%Y %H:%M:%S", &tme);
	time_t mt = mktime(&tme);
	ubuf.modtime = mt;
	ubuf.actime = 0;

	if (utime(filename, &ubuf) != 0)
	{
		perror("utime() error");
	}
}

char * getClientInfo(struct sockaddr_storage client_addr)
{
	printf("try get clients hostname");
	struct hostent *host;
	if (client_addr.ss_family == AF_INET)
	{
		struct sockaddr_in6* ipV6Addr = (struct sockaddr_in6*) &client_addr;
		struct in6_addr ipAddr = ipV6Addr->sin6_addr;
		host = gethostbyaddr(&ipAddr, sizeof(ipAddr), AF_INET);

	}
	else
	{
		struct sockaddr_in* ipV4Addr = (struct sockaddr_in*) &client_addr;
		struct in_addr ipAddr = ipV4Addr->sin_addr;
		host = gethostbyaddr(&ipAddr, sizeof(ipAddr), AF_INET6);
	}
	printf("HOSTNAME: %s \n ", host->h_name);
	return util::concat(4, host->h_name,":",SRV_PORT, ",");
//	if (((struct sockaddr)client_addr).sa_family == AF_INET6){
//		struct sockaddr_in6* ipV6Addr = (struct sockaddr_in6*)&client_addr;
//		struct in6_addr ipAddr       = ipV6Addr->sin6_addr;
//		struct hostent *host = gethostbyaddr(ipAddr);
//		return host->h_name;
//	}else if (((struct sockaddr)client_addr).sa_family == AF_INET){
//		struct sockaddr_in* ipV4Addr = (struct sockaddr_in*)&client_addr;
//		struct in_addr ipAddr = ipV4Addr->sin_addr;
//		struct hostent *host = gethostbyaddr(ipAddr);
//		return host->h_name;
//	}

//char str[INET6_ADDRSTRLEN];
//inet_ntop( AF_INET6, &ipAddr, str, INET6_ADDRSTRLEN );
//return str;
}

//clients
int c_desc[NUMBER_OF_CLIENTS];
struct sockaddr_storage c_addr[NUMBER_OF_CLIENTS];
void addClient(int socket_desc, struct sockaddr_storage addr)
{
	int i;
	printf("try add Client\n");
	for (i = 0; i < NUMBER_OF_CLIENTS; i++)
	{
		if (c_desc[i] == 0)
		{
			c_desc[i] = socket_desc;
			c_addr[i] = addr;
			printf("added Client\n");
			return;
		}
		else
		{
			continue;
		}
	}
	printf("ERROR: addClient, no free space!");
}

void deleteClient(int socket_desc)
{
	int i;
	printf("deleteClient");
	for (i = 0; i < NUMBER_OF_CLIENTS; i++)
	{
		if (c_desc[i] == socket_desc)
		{
			c_desc[i] = 0;
			return;
		}
	}
	printf("ERROR: deleteClient, no free space!");
}

typedef struct sockaddr_storage sockaddr_storeage;
int main()
{
	int s_tcp, c_tcp; /* socket descriptor */
	char info[256];
	memset(info, 0, sizeof(info));
	struct addrinfo hints, *servArr, *serv;
	sockaddr_storage address_client;
	std::map<int, sockaddr_storeage> clients;
	std::map<int, sockaddr_storeage>::iterator it;

	memset(c_desc, 0, sizeof(c_desc));
	memset(c_addr, 0, sizeof(c_addr));

	fd_set client_set, temp_client_set, list_temp_set;
	int fdmax;
	int option = 1;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET6;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	int errnumber = getaddrinfo(NULL, SRV_PORT, &hints, &servArr);
	if (errnumber != 0)
	{
		perror(gai_strerror(errnumber));
		exit(1);
	}

	for (serv = servArr; serv != NULL; serv = serv->ai_next)
	{
		s_tcp = socket(serv->ai_family, serv->ai_socktype, serv->ai_protocol);
		if (s_tcp == -1)
			continue;

		setsockopt(s_tcp, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));

		if (bind(s_tcp, serv->ai_addr, serv->ai_addrlen) == 0)
			break; /* Success */

	}
	if (serv == NULL)
	{
		printf("error binding");
		exit(1);
	}
	char addr_str[INET6_ADDRSTRLEN];
	get_ip_str(serv->ai_addr, addr_str, INET6_ADDRSTRLEN);

	freeaddrinfo(servArr);

	if (listen(s_tcp, NUMBER_OF_CLIENTS) < 0)
	{
		perror("listen");
		close(s_tcp);
		exit(1);
	}

	FD_ZERO(&client_set);
	FD_SET(s_tcp, &client_set);

	//add socked descriptor to set
	fdmax = s_tcp;

	while (1)
	{
		FD_ZERO(&temp_client_set);
		FD_ZERO(&list_temp_set);
		list_temp_set = client_set;
		temp_client_set = client_set;
		if (select(fdmax + 1, &temp_client_set, NULL, NULL, NULL) < 0)
		{
			perror("Selection error");
			close(s_tcp);
			exit(1);
		}
		int i;
		for (i = 0; i <= fdmax; i++)
		{
			if (FD_ISSET(i, &temp_client_set))
			{
				if (i == s_tcp)
				{ //new connection
					uint sizeAddClient = sizeof address_client;
					c_tcp = accept(s_tcp, (struct sockaddr*) &address_client,
							&sizeAddClient);
					if (c_tcp < 0)
					{
						perror("accept");
						close(s_tcp);
						exit(1);
					}
					else
					{
						FD_SET(c_tcp, &client_set);
						clients.insert(std::make_pair(c_tcp, address_client));
//						char *clientIp = getClientsIPAddress(address_client);
//						printf("got connected to %s \n", clientIp); //TODO IPV4 vs IPV6
						//add socked descriptor to set
						if (c_tcp > fdmax)
						{
							fdmax = c_tcp;
						}
					}
				}
				else
				{ //handle client
				  //read
					printf("got message \n");
					memset(info, 0, sizeof(info));
					printf("1");
					int read = recv(i, info, sizeof(info), 0);
					printf("2");

					//reply
					if (read > 0)
					{
						printf("3");
						char *cmd = strtok(info, ",");
						char *receivedFileInfo = strtok(NULL, "");
						int n;
						printf(cmd);

						if (strcmp(cmd, "List") == 0)
						{
							char *host;
							printf("getting hostinfo");
							for (it = clients.begin(); it != clients.end(); it++)
							{
								printf("getting hostinfo");
								host = getClientInfo(it->second);
								if ((n = send(i, host, strlen(host), 0)) > 0)
								{
									printf("Send hostname: %s - ( %i/%i Bytes).\n",
											host, n, strlen(host));
								}
								free(host);
							}


//
//
//
//							for (j=0; j<=fdmax;j++){
//								if(i == s_tcp){
//									FD_CLR(i, &list_temp_set);
//								}
//								if (FD_ISSET(j, &list_temp_set)) {
////									struct sockaddr_in6* pV6Addr = (struct sockaddr_in6*)&client_addr;
////									struct in6_addr ipAddr       = pV6Addr->sin6_addr;
//
//
//									char* hostname = getClientHostName(&address_client);
////									response = concat(4,hostname,":",SRV_PORT, ",");
////									if ((n = send(i, response,strlen(response), 0)) > 0) {
////										printf("Textfile %s sent ( %i/%i Bytes).\n",response, n, strlen(response));
////									}
////								}
//							}
//							free (response);
						}
						else if (strcmp(cmd, "Quit") == 0)
						{
							// brauchen wir eine antwort zum client? der schaltet doch ab
							printf("Quitting Conenction");
							char *response = "Connection Quit";
							if ((n = send(i, response, strlen(response), 0)) > 0)
							{
								printf("Textfile %s sent ( %i/%i Bytes).\n", response, n,
										strlen(response));
							}
							FD_CLR(i, &client_set);
							clients.erase(i);
							close(i);
						}
						else if (strcmp(cmd, "Get") == 0)
						{
							if (receivedFileInfo == NULL)
							{
								printf("Error: didnt received a valid filename\n");
							}
							char *fileInfo = util::getFileInfo(receivedFileInfo);
							char *response;
							if (fileInfo == NULL)
							{
								continue;
							}
							response = util::concat(3, "Datei-Attribute:", ",", fileInfo);
							if ((n = send(i, response, strlen(response), 0)) > 0)
							{
								printf("Textfile %s sent ( %i/%i Bytes).\n", response, n,
										strlen(response));
							}
							free(fileInfo);
							free(response);
						}
						else if (strcmp(cmd, "Put") == 0)
						{
							char *response;
							char *time;
							char hostname[30];
							gethostname(hostname, sizeof hostname);
							saveTxtAsFile(receivedFileInfo);
							time = getCurrentTime();

							response = util::concat(7, "Ok", ",", hostname, ",", addr_str,
									",", time);
							printf(response);
							if ((n = send(i, response, strlen(response), 0)) > 0)
							{
								printf("Message sent ( %d Bytes).\n", n);
							}

							free(response);
							free(time);

						}
						else
						{
							printf("Server received undefined message:\n%s", info);
						}
					}

					//close(i);
					//FD_CLR(i, &client_set);
					//remove from set
				}
			}
		}
	}

}
