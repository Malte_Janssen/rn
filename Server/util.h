/*
 * util.h
 *
 *  Created on: 06.06.2018
 *      Author: bs
 */

#ifndef UTIL_H_
#define UTIL_H_

#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>
#include <sys/stat.h>
#include <stdio.h>

/**
 * Concantinates a variable number of strings
 * allocates memory!
 */
char* concat(int count, ...);
char *getFileInfo(char*);

#endif /* UTIL_H_ */
